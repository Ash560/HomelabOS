# HealthChecks

[HealthChecks](HealthChecks.io) A Cron Monitoring Tool written in Python & Django https://healthchecks.io

## Access

It is available at [https://healthchecks.{{ domain }}/](https://healthchecks.{{ domain }}/) or [http://healthchecks.{{ domain }}/](http://healthchecks.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://healthchecks.{{ tor_domain }}/](http://healthchecks.{{ tor_domain }}/)
{% endif %}
